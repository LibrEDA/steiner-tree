// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
// SPDX-FileCopyrightText: 2022 Axel Viala
//
// SPDX-License-Identifier: GPL-3.0-or-later

use steiner_tree::gen_lut;

use criterion::{black_box, criterion_group, criterion_main, Criterion};

/// Benchmark LUT generation.
fn bench_gen_lut_full(c: &mut Criterion) {
    c.bench_function("gen_lut_full 6", |b| {
        b.iter(|| gen_lut::gen_full_lut(black_box(6)))
    });
    c.bench_function("gen_lut_full 7", |b| {
        b.iter(|| gen_lut::gen_full_lut(black_box(7)))
    });
}

/// Benchmark computation of steiner-trees (excluding LUT generation).
fn bench_rsmt_low_degree(c: &mut Criterion) {
    // Pre-compute the lookup-table.
    let max_degree = 8;
    let lut = gen_lut::gen_full_lut(max_degree);
    // Generate some points.
    let points: Vec<_> = (0..max_degree)
        .zip(0..max_degree)
        .take(max_degree)
        .map(|p| p.into())
        .collect();

    c.bench_function("bench_rsmt_low_degree 8", |b| {
        b.iter(|| {
            let points = points.clone();
            let (_rsmt, _wl) = black_box(&lut).rsmt_low_degree(black_box(points));
        })
    });
}

criterion_group!(benches, bench_gen_lut_full, bench_rsmt_low_degree);
criterion_main!(benches);

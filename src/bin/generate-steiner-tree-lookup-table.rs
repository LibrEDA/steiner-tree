// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::time;

use steiner_tree::gen_lut;
use steiner_tree::serialization;

fn main() {
    println!("Generate lookup-table for fast rectilinear minimal steiner tree construction.");

    let lut = {
        let degree = 7;
        println!("Generate lookup table up to degree {degree}");
        let now = time::Instant::now();
        let lut = gen_lut::gen_full_lut(degree);
        let elapsed = now.elapsed();
        lut.print_statistics();
        println!("Done in {} ms", elapsed.as_millis());
        lut
    };

    println!("Serialize LUT...");
    let mut buffer = Vec::new();

    let write_result = serialization::write_lut(&mut buffer, &lut);
    assert!(write_result.is_ok());
    println!("LUT binary size: {}", buffer.len());
}

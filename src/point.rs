// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use num_traits::{Signed, Zero};
use std::ops::{Add, Sub};

/// Point in the Euclidean plane.
#[derive(Copy, Clone, Debug, Hash, PartialEq, Eq, Ord, PartialOrd)]
pub struct Point<T> {
    /// x coordinate.
    pub x: T,
    /// y coordinate.
    pub y: T,
}

impl<T> Point<T> {
    /// Create a new point.
    pub fn new(x: T, y: T) -> Self {
        Self { x, y }
    }
}

impl<T> Point<T>
where
    T: Copy + Add<Output = T> + Sub<Output = T> + Zero,
{
    /// Rotate the point around the origin by 90 degrees counter-clock-wise.
    pub fn rotate_ccw90(&self) -> Self {
        Self::new(T::zero() - self.y, self.x)
    }
}

impl<T> Point<T>
where
    T: Copy + Add<Output = T> + Sub<Output = T> + Signed,
{
    /// Compute the manhattan distance between both points.
    pub fn manhattan_distance(&self, other: &Self) -> T {
        let dx = self.x - other.x;
        let dy = self.y - other.y;

        dx.abs() + dy.abs()
    }

    /// Shift the point.
    pub fn translate(&self, (dx, dy): (T, T)) -> Self {
        Point::new(self.x + dx, self.y + dy)
    }
}

impl<T> From<(T, T)> for Point<T> {
    fn from((x, y): (T, T)) -> Self {
        Point::new(x, y)
    }
}

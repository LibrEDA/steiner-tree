<!--
SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# steiner-tree

Fast lookup-table based computation of rectilinear Steiner minimal trees (RSMT).

This crate implements the 'FLUTE' algoritm and includes generating the lookup-tables
as well as creating trees based on the lookup tables.
Lookup-tables for up to 9 pins can be generated withing 16 seconds.

For higher pin count, a net-breaking heuristic is already implemented but yet inferior to the FLUTE 3.0 algorithm.

# Benchmarks
A simple benchmarking infrastructure can be used with `cargo bench`.

# TODOs

[ ] Implement net-breaking algorithms from FLUTE 3.0.
[ ] Read/write lookup-tables. Generating lookup-tables of degree larger than 8 is too slow to do on the fly.
